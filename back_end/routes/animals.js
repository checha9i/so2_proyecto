var express = require("express");
var router = express.Router();

const { MongoClient } = require("mongodb");
const uri =
  "mongodb+srv://admin:admin@cluster0.lza73.mongodb.net/ia_proyecto?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const fs = require("fs");
const { get } = require("http");
const prolog = require("../prolog");
/* GET users listing. */
router.get("/", function (req, res, next) {
  MongoClient.connect(uri, function (err, db) {
    if (err) throw err;
    var dbo = db.db("ia_proyecto");
    resultado = [];
    dbo
      .collection("animals")
      .find({})
      .toArray(function (err, result) {
        if (err) throw err;
        //console.log(result);
        resultado = result;
        db.close();
        res.send(resultado);
      });
  });

  /*var salida = prueba("america(X).");

  setTimeout(() => {
    res.send(salida);
  }, 2000);*/
});

router.post("/filtrado", async function (req, res, next) {
  var query = "buscar(X) :- ";
  var coma = false;
  req.body.tags.forEach((tag) => {
    if (coma === true) {
      query += ", ";
    }
    switch (tag) {
      case "Vertebrado":
        query += "vertebrado(X)";
        break;
      case "Invertebrado":
        query += "invertebrado(X)";
        break;
      case "Molusco":
        query += "molusco(X, _, _, _, _, _, _, _)";
        break;
      case "Pez":
        query += "pez(X, _, _, _, _, _, _, _)";
        break;
      case "Reptil":
        query += "reptil(X, _, _, _, _, _, _, _)";
        break;
      case "Ave":
        query += "ave(X, _, _, _, _, _, _, _)";
        break;
      case "Mamifero":
        query += "mamifero(X, _, _, _, _, _, _, _)";
        break;
      case "Sangre Fria":
        query += "sangrefria(X)";
        break;
      case "Sangre Caliente":
        query += "sangrecaliente(X)";
        break;
      case "Terrestre":
        query += "terrestre(X)";
        break;
      case "Marino":
        query += "marino(X)";
        break;
      case "Escamas":
        query += "escamas(X)";
        break;
      case "Alas":
        query += "alas(X)";
        break;
      case "Cola":
        query += "cola(X)";
        break;
      case "Vuela":
        query += "vuela(X)";
        break;
      case "Toma leche":
        query += "tomaleche(X)";
        break;
      case "Longevidad Baja":
        query += "longevidadbaja(X)";
        break;
      case "Longevidad Media":
        query += "longevidadmedia(X)";
        break;
      case "Longevidad Alta":
        query += "longevidadalta(X)";
        break;
      case "Extinto":
        query += "extinto(X)";
        break;
      case "Vulnerable":
        query += "vulnerable(X)";
        break;
      case "Animal Grande":
        query += "animalgrande(X)";
        break;
      case "Animal Pequeño":
        query += "animalpequeno(X)";
        break;
      case "No esta vulnerable":
        query += "novulnerable(X)";
        break;
      case "No vuela":
        query += "novuela(X)";
        break;
      case "Animal Rapido":
        query += "animalrapido(X)";
        break;
      case "Animal Lento":
        query += "animallento(X)";
        break;
      case "Velocidad Normal":
        query += "velocidadnormal(X)";
        break;
      case "Tiene Vida":
        query += "tienevida(X)";
        break;
      case "Respira":
        query += "respira(X)";
        break;
      case "Siente":
        query += "siente(X)";
        break;
    }
    coma = true;
  });
  query += ".";
  //console.log(query);
  var salida =await prolog.prueba(query);
  res.send(salida);
  /*setTimeout(function () {
    //console.log(salida);
    
    //res.send(salida);
  }, 2000);*/
});

router.get("/caracteristicas", function (req, res, next) {
  MongoClient.connect(uri, function (err, db) {
    if (err) throw err;
    var dbo = db.db("ia_proyecto");
    resultado = [];
    dbo
      .collection("caracteristicas")
      .find({})
      .toArray(function (err, result) {
        if (err) throw err;
        //console.log(result);
        resultado = result;
        db.close();
        res.send(resultado);
      });
  });
});

router.post("/exist",async function (req, res, next) {
  var query = "buscar(X) :- ";
  var coma = false;
  req.body.tags.forEach((tag) => {
    if (coma === true) {
      query += ", ";
    }
    switch (tag) {
      case "Vertebrado":
        query += "vertebrado(X)";
        break;
      case "Invertebrado":
        query += "invertebrado(X)";
        break;
      case "Molusco":
        query += "molusco(X, _, _, _, _, _, _, _)";
        break;
      case "Pez":
        query += "pez(X, _, _, _, _, _, _, _)";
        break;
      case "Reptil":
        query += "reptil(X, _, _, _, _, _, _, _)";
        break;
      case "Ave":
        query += "ave(X, _, _, _, _, _, _, _)";
        break;
      case "Mamifero":
        query += "mamifero(X, _, _, _, _, _, _, _)";
        break;
      case "Sangre Fria":
        query += "sangrefria(X)";
        break;
      case "Sangre Caliente":
        query += "sangrecaliente(X)";
        break;
      case "Terrestre":
        query += "terrestre(X)";
        break;
      case "Marino":
        query += "marino(X)";
        break;
      case "Escamas":
        query += "escamas(X)";
        break;
      case "Alas":
        query += "alas(X)";
        break;
      case "Cola":
        query += "cola(X)";
        break;
      case "Vuela":
        query += "vuela(X)";
        break;
      case "Toma leche":
        query += "tomaleche(X)";
        break;
      case "Longevidad Baja":
        query += "longevidadbaja(X)";
        break;
      case "Longevidad Media":
        query += "longevidadmedia(X)";
        break;
      case "Longevidad Alta":
        query += "longevidadalta(X)";
        break;
      case "Extinto":
        query += "extinto(X)";
        break;
      case "Vulnerable":
        query += "vulnerable(X)";
        break;
      case "Animal Grande":
        query += "animalgrande(X)";
        break;
      case "Animal Pequeño":
        query += "animalpequeno(X)";
        break;
      case "No esta vulnerable":
        query += "novulnerable(X)";
        break;
      case "No vuela":
        query += "novuela(X)";
        break;
      case "Animal Rapido":
        query += "animalrapido(X)";
        break;
      case "Animal Lento":
        query += "animallento(X)";
        break;
      case "Velocidad Normal":
        query += "velocidadnormal(X)";
        break;
      case "Tiene Vida":
        query += "tienevida(X)";
        break;
      case "Respira":
        query += "respira(X)";
        break;
      case "Siente":
        query += "siente(X)";
        break;
    }
    coma = true;
  });
  query += ".";
  //console.log(query);
  var salida = await prolog.prueba(query);

  //setTimeout(function () {
    //console.log(salida);
    var existe = false;
    salida.forEach((animal) => {
      if (animal === req.body.animal) {
        existe = true;
        res.send("SI");
      }
    });
    if (!existe) res.send("NO");
    //res.send(salida);
  //}, 2000);
});

async function getAnimals() {
  try {
    await client.connect();

    await listDatabases(client);
  } catch (e) {
    console.error(e);
  } finally {
    await client.close();
  }
}

async function listDatabases(client) {
  databasesList = await client.db().admin().listDatabases();

  console.log("Databases:");
  databasesList.databases.forEach((db) => console.log(` - ${db.name}`));
}

module.exports = router;
