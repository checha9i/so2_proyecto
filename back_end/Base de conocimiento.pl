
pez(salmon,1.5,46.8,13,"salmo salar linnaeus",america,1200000,1.805556).
pez(tiburon,4.1,1100,272,"carcharodon carcharias",[america, africa,asia,oceania],2000,15.55556).
pez(anguila,0.8,3.6,50,"anguilla anguilla",[europa,america],10000,1.388889).
pez(pezpayaso, 0.1, 0.005, 10, "Amphiprioninae", [asia,america], 1000000, 0.98).
pez(pezglobo, 0.1, 0.010, 7, "Tetraodontidae", [asia,america,africa,europa], 5000000, 0.99).

/* PECES */

/*  REPTILES */
reptil(anaconda, 4.6,80,20,"Eunectes marinus",[america, africa], 200000, 4).
reptil(cocodrilo, 5.2, 1000, 70, "Crocodylidae", [africa, asia, america], 67500, 8).
reptil(lagartija, 0.010, 0.005, 5, "Lacertilia", [america], 12000000, 2.7).
reptil(tortuga, 1, 160, 150, "Chelonioidea", [america, europa], 1000000, 9.72).
reptil(caiman, 2.4, 7.6, 30, "Caiman", [america, africa], 50000, 4.72).



/*  MOLUSCOS */
molusco(calamar, 0.6, 750, 15, "Teuthida", [asia, europa], 12000000, 2.5).
molusco(caracol, 0.02, 0.005, 7, "Cornu Aspersum", [america, asia, europa], 21500000, 0.013).
molusco(pulpo, 1.2, 50, 5, "Octopoda", [asia, america], 5036500, 9.4).
molusco(sepia, 0.9, 25, 6, "Sepiida", [asia, europa], 965425, 20).
molusco(babosa, 0.005, 0.015, 2, "Mollusca",[america, europa], 9665522, 0.015).

/*  AVES */
ave(canario, 0.017, 0.024, 10, "serinus canaria domestica", [africa, asia], 90000, 5.55).
ave(gaviota, 0.062, 0.96, 15, "larus armenicus", [europa, america], 1200000, 12.5).
ave(buho, 0.71, 2.7, 30, "bubo bobo", [asia,america, europa], 28000, 22.22).
ave(carpintero, 0.17, 0.075, 10, "picidos ", [america,europa], 36000, 6.9).
ave(loro, 0.033, 0.04, 11, "psittacoidea", [america,europa], 89000, 6.6).

/* MAMIFEROS */
mamifero(ballena, 15, 23000, 70, "balaenidae", [europa,america, asia], 15000, 13.33).
mamifero(caballo, 1.8, 1000, 30, "equus caballus", [america], 89000, 24.44).
mamifero(gato, 0.3, 4.5, 16, "felis catus", [america], 3000000, 13.88).
mamifero(jirafa, 6.1, 1600, 25, "giraffidae", [africa], 1000000, 16.66).
mamifero(nutria, 0.95, 5.9, 9, "lutrinae", [america], 1600000, 3.33).


/* AMERICA */
america(salmon).
america(tiburon).
america(anguila).
america(pezpayaso).
america(pezglobo).
america(anaconda).
america(cocodrilo).
america(lagartija).
america(tortuga).
america(caiman).
america(caracol).
america(pulpo).
america(babosa).
america(gaviota).
america(buho).
america(carpintero).
america(loro).
america(ballena).
america(caballo).
america(gato).
america(nutria).

/* OCEANIA */
oceania(tiburon).

/* ASIA */

asia(ballena).
asia(buho).
asia(canario).
asia(sepia).
asia(pulpo).
asia(caracol).
asia(calamar).
asia(cocodrilo).
asia(pzglobo).
asia(pezpayaso).
asia(tiburon).
asia(tiburon).

/* AFRICA */
africa(jirafa).
africa(canario).
africa(caiman).
africa(cocodrilo).
africa(anaconda).
africa(pzglobo).

/* EUROPA */
europa(ballena).
europa(loro).
europa(carpintero).
europa(buho).
europa(gaviota).
europa(babosa).
europa(sepia).
europa(caracol).
europa(calamar).
europa(tortuga).
europa(pzglobo).
europa(anguila).

/* CONTINENTES */
continente(america).
continente(europa).
continente(oceania).
continente(asia).
continente(africa).

/* REGLAS */
/* No.1 */
sangrefria(X) :- reptil(X, _, _, _, _, _, _, _).
sangrefria(X) :- molusco(X, _, _, _, _, _, _, _).
sangrefria(X) :- pez(X, _, _, _, _, _, _, _).

cola(X) :- reptil(X, _, _, _, _, _, _, _).
cola(X) :- pez(X, _, _, _, _, _, _, _).
cola(X):- mamifero(X, _, _, _, _, _, _, _).
cola(X) :- ave(X, _, _, _, _, _, _, _).

/* No.2 */
terrestre(caracol).
terrestre(babosa).
terrestre(cocodrilo).
terrestre(caiman).
terrestre(anaconda).
terrestre(lagartija).
terrestre(canario).
terrestre(gaviota).
terrestre(buho).
terrestre(carpintero).
terrestre(loro).
terrestre(caballo).
terrestre(gato).
terrestre(jirafa).
terrestre(nutria).

marino(pulpo).
marino(calamar).
marino(sepia).
marino(tortuga).
marino(ballena).
marino(X) :- pez(X, _, _, _, _, _, _, _).
/* No.3 */

escamas(X) :- reptil(X, _, _, _, _, _, _, _).
escamas(X) :- pez(X, _, _, _, _, _, _, _).

/* No.4

viveenagua(X) :- pez(X, _, _, _, _, _, _, _).
viveenagua(X) :- molusco(X, _, _, _, _, _, _, _), X == calamar.
viveenagua(X) :- molusco(X, _, _, _, _, _, _, _), X == pulpo.
viveenagua(X) :- molusco(X, _, _, _, _, _, _, _), X == sepia.
 No.5 */
oviparo(X) :- pez(X, _, _, _, _, _, _, _).
oviparo(X) :- reptil(X, _, _, _, _, _, _, _).
oviparo(X) :- ave(X, _, _, _, _, _, _, _).
oviparo(X) :- molusco(X, _, _, _, _, _, _, _).

/* No.6 */

alas(X) :- ave(X, _, _, _, _, _, _, _).

/* No.7 */

vuela(X) :- ave(X, _, _, _, _, _, _, _).
novuela(X) :- reptil(X, _, _, _, _, _, _, _).
novuela(X) :- molusco(X, _, _, _, _, _, _, _).
novuela(X) :- mamifero(X, _, _, _, _, _, _, _).
novuela(X) :- pez(X, _, _, _, _, _, _, _).
/* No.8 */

sangrecaliente(X) :- mamifero(X, _, _, _, _, _, _, _).
sangrecaliente(X) :- ave(X, _, _, _, _, _, _, _).

/* No.9 */

tomaleche(X) :- mamifero(X, _, _, _, _, _, _, _).

/* No.10 */

longevidadbaja(X) :- pez(X, _, _, Y, _, _, _, _), Y < 10.
longevidadbaja(X) :- molusco(X, _, _, Y, _, _, _, _), Y < 10.
longevidadbaja(X) :- reptil(X, _, _, Y, _, _, _, _), Y < 10.
longevidadbaja(X) :- ave(X, _, _, Y, _, _, _, _), Y < 10.
longevidadbaja(X) :- mamifero(X, _, _, Y, _, _, _, _), Y < 10.

/* No.11 */

longevidadmedia(X) :- pez(X, _, _, Y, _, _, _, _), Y > 9, Y =< 60.
longevidadmedia(X) :- molusco(X, _, _, Y, _, _, _, _), Y > 9, Y =< 60.
longevidadmedia(X) :- reptil(X, _, _, Y, _, _, _, _), Y > 9, Y =< 60.
longevidadmedia(X) :- ave(X, _, _, Y, _, _, _, _), Y > 9, Y =< 60.
longevidadmedia(X) :- mamifero(X, _, _, Y, _, _, _, _), Y > 9, Y =< 60.

/* No.12 */
longevidadalta(X) :- mamifero(X, _, _, Y, _, _, _, _), Y > 60.
longevidadalta(X) :- ave(X, _, _, Y, _, _, _, _), Y > 60.
longevidadalta(X) :- pez(X, _, _, Y, _, _, _, _), Y > 60.
longevidadalta(X) :- reptil(X, _, _, Y, _, _, _, _), Y > 60.
longevidadalta(X) :- molusco(X, _, _, Y, _, _, _, _), Y > 60.

/* No.13 */

extinto(X) :- mamifero(X, _, _, _, _, _, Y, _), Y == 0.
extinto(X) :- pez(X, _, _, _, _, _, Y, _), Y == 0.
extinto(X) :- ave(X, _, _, _, _, _, Y, _), Y == 0.
extinto(X) :- reptil(X, _, _, _, _, _, Y, _), Y == 0.
extinto(X) :- molusco(X, _, _, _, _, _, Y, _), Y == 0.


/* No.14 */
vulnerable(X) :- mamifero(X, _, _, _, _, _, Y, _), Y < 5000.
vulnerable(X) :- pez(X, _, _, _, _, _, Y, _), Y < 5000.
vulnerable(X) :- ave(X, _, _, _, _, _, Y, _), Y < 5000.
vulnerable(X) :- reptil(X, _, _, _, _, _, Y, _), Y < 5000.
vulnerable(X) :- molusco(X, _, _, _, _, _, Y, _), Y < 5000.

novulnerable(X) :- mamifero(X, _, _, _, _, _, Y, _), Y >= 5000.
novulnerable(X) :- pez(X, _, _, _, _, _, Y, _), Y >= 5000.
novulnerable(X) :- ave(X, _, _, _, _, _, Y, _), Y >= 5000.
novulnerable(X) :- reptil(X, _, _, _, _, _, Y, _), Y >= 5000.
novulnerable(X) :- molusco(X, _, _, _, _, _, Y, _), Y >= 5000.

/* No.15 */
animalgrande(X) :- pez(X,Y,Z,_,_,_,_,_), Y>=1.
animalgrande(X) :- pez(X,_,Z,_,_,_,_,_), Z>=50.
animalgrande(X) :- mamifero(X,Y,_,_,_,_,_,_), Y>=1.
animalgrande(X) :- mamifero(X,_,Z,_,_,_,_,_), Z>=50.
animalgrande(X) :- reptil(X,Y,_,_,_,_,_,_), Y>=1.
animalgrande(X) :- reptil(X,_,Z,_,_,_,_,_), Z>=50.
animalgrande(X) :- molusco(X,Y,_,_,_,_,_,_), Y>=1.
animalgrande(X) :- molusco(X,_,Z,_,_,_,_,_), Z>=50.
animalgrande(X) :- ave(X,Y,_,_,_,_,_,_), Y>=1.
animalgrande(X) :- ave(X,_,Z,_,_,_,_,_),  Z>=50.

animalpequeno(X) :- pez(X,Y,Z,_,_,_,_,_), Y<1, Z<50.
animalpequeno(X) :- mamifero(X,Y,Z,_,_,_,_,_), Y<1, Z<50.
animalpequeno(X) :- reptil(X,Y,Z,_,_,_,_,_), Y<1, Z<50.
animalpequeno(X) :- molusco(X,Y,Z,_,_,_,_,_), Y<1, Z<50.
animalpequeno(X) :- ave(X,Y,Z,_,_,_,_,_), Y<1,Z< 50.

/* No.16 */

animallento(X) :- ave(X,_,_,_,_,_,_,Y), Y<1.
animallento(X) :- mamifero(X,_,_,_,_,_,_,Y), Y<1.
animallento(X) :- reptil(X,_,_,_,_,_,_,Y), Y<1.
animallento(X) :- molusco(X,_,_,_,_,_,_,Y), Y<1.
animallento(X) :- pez(X,_,_,_,_,_,_,Y), Y<1.

/* No.17 */
animalrapido(X) :- pez(X,_,_,_,_,_,_,Y), Y>10.
animalrapido(X) :- mamifero(X,_,_,_,_,_,_,Y), Y>10.
animalrapido(X) :- reptil(X,_,_,_,_,_,_,Y), Y>10.
animalrapido(X) :- molusco(X,_,_,_,_,_,_,Y), Y>10.
animalrapido(X) :- ave(X,_,_,_,_,_,_,Y), Y>10.

velocidadnormal(X) :- pez(X,_,_,_,_,_,_,Y), Y>=1, Y< 11.
velocidadnormal(X) :- mamifero(X,_,_,_,_,_,_,Y), Y>=1, Y<11.
velocidadnormal(X) :- reptil(X,_,_,_,_,_,_,Y), Y>=1, Y<11.
velocidadnormal(X) :- molusco(X,_,_,_,_,_,_,Y), Y>=1, Y<11.
velocidadnormal(X) :- ave(X,_,_,_,_,_,_,Y), Y>=1,Y<11.

/* No.16 */

tienevida(X) :- ave(X,_,_,_,_,_,_,_).
tienevida(X) :- pez(X,_,_,_,_,_,_,_).
tienevida(X) :- mamifero(X,_,_,_,_,_,_,_).
tienevida(X) :- molusco(X,_,_,_,_,_,_,_).
tienevida(X) :- reptil(X,_,_,_,_,_,_,_).

/* No.16 */

respira(X) :- ave(X,_,_,_,_,_,_,_).
respira(X) :- pez(X,_,_,_,_,_,_,_).
respira(X) :- mamifero(X,_,_,_,_,_,_,_).
respira(X) :- molusco(X,_,_,_,_,_,_,_).
respira(X) :- reptil(X,_,_,_,_,_,_,_).

invertebrado(X) :- reptil(X,_,_,_,_,_,_,_), X == anaconda.
invertebrado(X) :- molusco(X,_,_,_,_,_,_,_).

vertebrado(X) :- pez(X,_,_,_,_,_,_,_).
vertebrado(X) :- ave(X,_,_,_,_,_,_,_).
vertebrado(X) :- reptil(X,_,_,_,_,_,_,_), X \== anaconda.
vertebrado(X) :- mamifero(X,_,_,_,_,_,_,_).

siente(X) :- ave(X,_,_,_,_,_,_,_).
siente(X) :- pez(X,_,_,_,_,_,_,_).
siente(X) :- mamifero(X,_,_,_,_,_,_,_).
siente(X) :- molusco(X,_,_,_,_,_,_,_).
siente(X) :- reptil(X,_,_,_,_,_,_,_).


animalespecial(X) :- sangrecaliente(X);sangrefria(X); not(ave(X,_,_,_,_,_,_,_)).

sobrepeso(X):- pez(X,Z,Y,_,_,_,_,_),(Y/Z**2)>25.
sobrepeso(X):- reptil(X,Z,Y,_,_,_,_,_),(Y/Z**2)>25.
sobrepeso(X):- molusco(X,Z,Y,_,_,_,_,_),(Y/Z**2)>25.
sobrepeso(X):- ave(X,Z,Y,_,_,_,_,_),(Y/Z**2)>25.
sobrepeso(X):- mamifero(X,Z,Y,_,_,_,_,_),(Y/Z**2)>25.